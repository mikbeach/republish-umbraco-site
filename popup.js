
chrome.tabs.query({'active': true, 'currentWindow': true}, function (tabs) {
    // grab the active chrome tab
    var url = tabs[0].url;
    // parse through purl
    var Purl = purl(url);
    //checks for https or http
    var protocol = Purl.attr('protocol') + "\://";
    //purl parses the URL to scrub out any subpages
    var host = Purl.attr('host');
    //purl find port ie localhost:8080
    var port = "\:" + Purl.attr('port');
    //the umbraco republish filepath
    var republishPath = "/umbraco/dialogs/republish.aspx?xml=true";
    
    //check to see if it's got a port number and knock out the currect url

    if (port != null) {
        var republishUrl = protocol+host+port+republishPath;
    }
    else {
        var republishUrl = protocol+host+republishPath;
    }
    
    //build the link
    var a = document.createElement('a');
    var text = document.createTextNode("republish this site");
    a.target = "blank";
    a.href = republishUrl;
    a.appendChild(text);
    document.body.appendChild(a);

    document.getElementById("text").innerHTML = host;


});
